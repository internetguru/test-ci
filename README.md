[![pipeline status](https://gitlab.com/internetguru/test-ci/badges/master/pipeline.svg)](https://gitlab.com/internetguru/test-ci/-/pipelines?ref=master)
[![compile](https://gitlab.com/internetguru/test-ci/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/internetguru/test-ci/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/internetguru/test-ci/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/internetguru/test-ci/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![iotest](https://gitlab.com/internetguru/test-ci/builds/artifacts/master/raw/.results/iotest.svg?job=evaluate)](https://gitlab.com/internetguru/test-ci/-/jobs/artifacts/master/file/.results/iotest.log?job=evaluate)

# Java Basics Tutorial

This tutorial introduces basic operations in the Java programming language: (1) printing output,  (2) reading input, and (3) using loops.

## Print Output

In Java, you can simply use following functions to send data to the standard output.

```java
System.out.println(); 
System.out.print();
System.out.printf();
```

Note:
The output test is case and white-space sensitive. That means upper/lower case matters as much as all other characters including the exclamation mark at the end of the string.

[Example 1](src/main/HelloWorld.java "code")

## Read Input

Java provides different ways to get input from the user. However, in this tutorial, you will learn to get input from user using the object of Scanner class.

In order to use the object of `Scanner`, we need to import `java.util.Scanner` package.

```java
import java.util.Scanner;
```

Then, we need to create an object of the `Scanner` class. We can use the object to take input from the user.

```java
// create an object of Scanner
Scanner input = new Scanner(System.in);
// take input from the user
int number = input.nextInt();

// do your code
// ...

// close the Scanner object
input.close();
```

[Example 2](src/main/Sum.java "code")

## While Loop

The `while loop` loops through a block of code as long as a specified condition is true.

```java
while (condition) {
  // code block to be executed
}
```

In the example below, the code in the loop will run, over and over again, as long as a variable `i` is less than 5:

```java
int i = 0;
while (i < 5) {
  System.out.println(i);
  i++;
}
```

Note:
To find out if there is another integer in the input, use `input.hasNextInt()` function in the `while` condition.

[Example 3](src/main/MultiSum.java "code")
